package test.agima.ru.calculate.opr;

/**
 * Created by 12max on 11.07.2017.
 */

public class Sum extends Operator {
    public Sum(){
        priority = 1;
    }

    @Override
    public void calc(Operator prev) {
        prev.setValue(prev.getValue() + value);
    }
}
