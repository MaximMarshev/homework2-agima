package test.agima.ru.calculate.opr;

/**
 * Created by 12max on 13.07.2017.
 */

public class Div extends Operator {
    public Div() {
        priority = 2;
    }

    @Override
    public void calc(Operator prev) {
        prev.setValue(prev.getValue() / value);
    }
}