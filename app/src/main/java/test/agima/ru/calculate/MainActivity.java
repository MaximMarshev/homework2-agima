package test.agima.ru.calculate;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import test.agima.ru.calculate.opr.Div;
import test.agima.ru.calculate.opr.Mlt;
import test.agima.ru.calculate.opr.Mns;
import test.agima.ru.calculate.opr.Operator;
import test.agima.ru.calculate.opr.Sum;

public class MainActivity extends AppCompatActivity {

    public static Context context;

    private ArrayList<Operator> list = new ArrayList();
    private TextView display;
    private TextView display2;

    boolean res = false;
    boolean znak = false;
    boolean repeat = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        display = (TextView) findViewById(R.id.display);
        display2 = (TextView) findViewById(R.id.display2);

        final Button btn_0 = (Button) findViewById(R.id.btn_0);
        btn_0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onNumberClick(((Button) view).getText());
            }
        });


        final Button btn_1 = (Button) findViewById(R.id.btn_1);
        btn_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onNumberClick(((Button) view).getText());
            }
        });

        final Button btn_2 = (Button) findViewById(R.id.btn_2);
        btn_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onNumberClick(((Button) view).getText());
            }
        });

        final Button btn_3 = (Button) findViewById(R.id.btn_3);
        btn_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onNumberClick(((Button) view).getText());
            }
        });

        final Button btn_4 = (Button) findViewById(R.id.btn_4);
        btn_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onNumberClick(((Button) view).getText());
            }
        });

        final Button btn_5 = (Button) findViewById(R.id.btn_5);
        btn_5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onNumberClick(((Button) view).getText());
            }
        });

        final Button btn_6 = (Button) findViewById(R.id.btn_6);
        btn_6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onNumberClick(((Button) view).getText());
            }
        });

        final Button btn_7 = (Button) findViewById(R.id.btn_7);
        btn_7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onNumberClick(((Button) view).getText());
            }
        });

        final Button btn_8 = (Button) findViewById(R.id.btn_8);
        btn_8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onNumberClick(((Button) view).getText());
            }
        });

        final Button btn_9 = (Button) findViewById(R.id.btn_9);
        btn_9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onNumberClick(((Button) view).getText());
            }
        });


        final Button btn_mlt = (Button) findViewById(R.id.btn_mlt);
        btn_mlt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onOperatorClick(((Button) view).getText().charAt(0));
            }
        });

        final Button btn_div = (Button) findViewById(R.id.btn_div);
        btn_div.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onOperatorClick(((Button) view).getText().charAt(0));
            }
        });

        final Button btn_pls = (Button) findViewById(R.id.btn_pls);
        btn_pls.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onOperatorClick(((Button) view).getText().charAt(0));
            }
        });

        final Button btn_mns = (Button) findViewById(R.id.btn_mns);
        btn_mns.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onOperatorClick(((Button) view).getText().charAt(0));
            }
        });


        final Button btn_clean = (Button) findViewById(R.id.btn_clean);
        btn_clean.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onOtherClick(((Button) view).getText().charAt(0));
            }
        });

        final Button btn_result = (Button) findViewById(R.id.btn_result);
        btn_result.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onOtherClick(((Button) view).getText().charAt(0));
            }
        });
    }

    private void calculate() {
        calc(2);
        calc(1);
        display.setText(String.valueOf(list.get(0).getValue()));
        display2.setText(display2.getText() + display.getText().toString());
    }

    private void calc(int priority) {

        for (int i = 1;i<list.size();++i) {
            if (priority == list.get(i).getPriority()) {
                list.get(i).calc(list.get(i - 1));
                list.remove(i);
                --i;
            }


        }

    }

    private void onOtherClick(char sign) {
        switch (sign) {
            case 'C':
                display.setText("");
                display2.setText("");
                list.clear();
                break;
            case '=':
                try {
                    list.get(list.size() - 1).setValue(Double.valueOf(display.getText().toString()));
                }
                catch (NumberFormatException e)
                {

                }
                if(!list.isEmpty()) {
                    res = true;
                    repeat = false;
                    display2.setText(display2.getText().toString() + sign);
                    //list.get(list.size()-1).setValue(Integer.valueOf(display.getText().toString()));

                    calculate();
                }
                break;
        }
    }

    private void onOperatorClick(char sign) {
        if (list.size() == 0) {
            list.add(new Operator());
        }
        try {
            list.get(list.size() - 1).setValue(Double.valueOf(display.getText().toString()));
        }
        catch (NumberFormatException e)
        {

        }

        switch (sign) {
            case '+':
                if(repeat)
                    list.set(list.size()-1, new Sum());
                else
                    list.add(new Sum());
                //repeat = true;
                if(res) {
                    res = false;
                    znak = true;
                }
                break;
            case '-':
                if(repeat)
                    list.set(list.size()-1, new Mns());
                else
                    list.add(new Mns());
               // repeat = true;
                if(res) {
                    res = false;
                    znak = true;
                }
                break;
            case '*':
                if(repeat)
                    list.set(list.size()-1, new Mlt());
                else
                    list.add(new Mlt());
                //repeat = true;
                if(res) {
                    res = false;
                    znak = true;
                }
                break;
            case '/':
                if(repeat)
                    list.set(list.size()-1, new Div());
                else
                    list.add(new Div());
                //repeat = true;
                if(res) {
                    res = false;
                    znak = true;
                }
                break;
        }
        if(repeat) {
            char c[] =display2.getText().toString().toCharArray();
            c[display2.getText().toString().length()-1]=sign;
            display2.setText(String.valueOf(c));
        }
        else
            display2.setText(display2.getText().toString() + sign);
        repeat = true;
        display.setText("");


    }

    private void onNumberClick(CharSequence text) {
        if(res&&znak) {
            display.setText("");
            display2.setText("");
            res = false;
            znak = false;
        }
        repeat = false;
        display.setText(display.getText() + text.toString());
        display2.setText(display2.getText() + text.toString());
    }
}




