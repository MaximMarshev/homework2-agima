package test.agima.ru.calculate.opr;

/**
 * Created by 12max on 13.07.2017.
 */

public class Mns extends Operator {
    public Mns(){
        priority = 1;
    }
    @Override
    public void calc(Operator prev){
        prev.setValue(prev.getValue() - value);
    }

}
